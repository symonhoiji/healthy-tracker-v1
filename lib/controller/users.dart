import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/components/bottom_bar.dart';
import 'package:healthy_tracker/model/users.dart';
import 'package:healthy_tracker/page/home.dart';

CollectionReference users = FirebaseFirestore.instance.collection('users');
User? user = FirebaseAuth.instance.currentUser;

Future<Users> getData() async {
  Users dataUser = new Users();
  await users.where('uid', isEqualTo: user!.uid).get().then((value) {
    value.docs.forEach((value) {
      dataUser.name = value['full_name'];
      dataUser.birthdate = value['birthdate'].toDate();
      dataUser.gender = value['gender'];
      dataUser.targetdrink = value['target_drinking'].toDouble();
      dataUser.uid = value['uid'];
      dataUser.weight = value['weight'].toDouble();
      dataUser.fweight = value['target_weight'].toDouble();
      dataUser.docId = value.id;
    });
  });
  return dataUser;
}

Future<String> getIdDocFromUserUID() async {
  String docId = '';
  await users.where('uid', isEqualTo: user!.uid).get().then((value) {
    value.docs.forEach((user) {
      docId = user.id;
    });
  });
  return docId;
}

Future<void> updateTargetDrinking(String docId, int n) async {
  return await users.doc(docId).update({'target_drinking': n});
}

Future<void> updateName(String docId, String name) async {
  return await users.doc(docId).update({'full_name': name});
}

Future<void> updateTargetWeight(String docId, double n) async {
  return await users.doc(docId).update({'target_weight': n});
}

Future<void> addUser(Users user) async {
  print(user);
  return await users
      .add({
        'uid': user.uid,
        'full_name': user.name,
        'gender': user.gender,
        'weight': user.weight,
        'target_weight': user.fweight,
        'height': user.height,
        'birthdate': user.birthdate,
        'stat_weight': [
          {'weight': user.weight, 'date': new DateTime.now()}
        ],
        'target_drinking': 2000,
      })
      .then((value) => print('User add'))
      .catchError((error) => print('Failed to add user: $error'));
}

Future<void> hasRegistered(String? id, BuildContext context) {
  return users.where('uid', isEqualTo: id).get().then((value) {
    if (value.size == 1) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => BottomBar()));
    }
  });
}
