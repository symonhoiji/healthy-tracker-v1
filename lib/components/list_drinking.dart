import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';

class ListDrinking extends StatelessWidget {
  final int quantity;
  const ListDrinking({Key? key, required this.quantity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 5.0),
      decoration: BoxDecoration(
          border: Border.all(width: 1.2, color: Colors.blue),
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          )),
      padding: EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(
                'assets/bottle.png',
                height: 90,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('drink.type'.tr(),
                      style: TextStyle(
                          fontSize: 28,
                          color: Colors.lightBlue,
                          fontWeight: FontWeight.bold)),
                  Text('$quantity ml',
                      style: TextStyle(
                          fontSize: 23,
                          color: Colors.grey[500],
                          fontWeight: FontWeight.w200)),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
