import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class WeightChartPage extends StatefulWidget {
  final List data;
  WeightChartPage({Key? key, required this.data}) : super(key: key);

  @override
  _WeightChartPageState createState() => _WeightChartPageState();
}

class _WeightChartPageState extends State<WeightChartPage> {
  List data = [];
  late ZoomPanBehavior _zoomPanBehavior;
  late TrackballBehavior _trackballBehavior;

  @override
  void initState() {
    super.initState();
    setSettingChart();
    data = widget.data;
  }

  void setSettingChart() {
    _zoomPanBehavior = ZoomPanBehavior(
      enableMouseWheelZooming: true,
    );
    _trackballBehavior = TrackballBehavior(
        // Enables the trackball
        enable: true,
        tooltipDisplayMode: TrackballDisplayMode.floatAllPoints,
        tooltipSettings: InteractiveTooltip(enable: true, color: Colors.red));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: SfCartesianChart(
            trackballBehavior: _trackballBehavior,
            zoomPanBehavior: _zoomPanBehavior,
            title: ChartTitle(text: 'stat.title'.tr()),
            // Initialize category axis
            primaryXAxis: CategoryAxis(visibleMaximum: 10, visibleMinimum: 0),
            series: <ChartSeries>[
              // Initialize line series
              AreaSeries<dynamic, DateTime>(
                dataSource: data,
                xValueMapper: (x, _) => x['date'].toDate(),
                yValueMapper: (y, _) => y['weight'],
                // Render the data label
                dataLabelSettings: DataLabelSettings(isVisible: true),
                markerSettings: MarkerSettings(isVisible: true),
              )
            ]),
      ),
    );
  }
}
