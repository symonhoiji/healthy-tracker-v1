import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/page/drinking.dart';
import 'package:healthy_tracker/page/home.dart';
import 'package:healthy_tracker/page/setting.dart';

class MyDrawer extends StatelessWidget {
  final _navigatorKey = GlobalKey<NavigatorState>();
  User? user = FirebaseAuth.instance.currentUser;
  final Widget body;
  final Widget title;
  final Widget? floatingButton;
  MyDrawer({required this.body, required this.title, this.floatingButton});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: floatingButton,
      appBar: AppBar(                                      
        title: title,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Image.network(
                  '${user!.photoURL}',
                ),
              ),
              accountName: Text('${user!.displayName}'),
              accountEmail: Text('(${user!.email})'),
            ),
          ],
        ),
      ),
      body: body,
    );
  }
}
