import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';

class BMIDetail extends StatelessWidget {
  final double bmi;

  Widget lineBMI({
    required Color color,
    BorderRadius? borderRadius,
    required String text,
    required String level,
  }) {
    return Column(
      children: [
        Text(
          level,
          style: TextStyle(fontSize: 12, color: color.withOpacity(0.8)),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(color: color, borderRadius: borderRadius),
          width: 60,
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          text,
          style: TextStyle(fontSize: 10, color: color.withOpacity(0.8)),
        ),
      ],
    );
  }

  const BMIDetail({Key? key, required this.bmi}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lineBMI(
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      bottomLeft: Radius.circular(8.0)),
                  level: '< 18.50',
                  text: 'home.bmi.level1'.tr()),
              SizedBox(
                width: 1,
              ),
              lineBMI(
                  color: Colors.green,
                  level: '18.50 - 23.99',
                  text: 'home.bmi.level2'.tr()),
              SizedBox(
                width: 1,
              ),
              lineBMI(
                  color: Colors.amber,
                  level: '24.00 - 29.99',
                  text: 'home.bmi.level3'.tr()),
              SizedBox(
                width: 1,
              ),
              lineBMI(
                  color: Colors.redAccent,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0)),
                  level: '> 30.00',
                  text: 'home.bmi.level4'.tr()),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text('home.bmi.detail'.tr())
        ],
      ),
    );
  }
}
