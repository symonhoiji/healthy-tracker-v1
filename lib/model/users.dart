class Users {
  String? docId;
  String? uid;
  String? name;
  String? gender;
  double? weight;
  double? fweight;
  double? height;
  double? targetdrink;
  DateTime? birthdate;

  Users(
      {this.docId,
      this.uid,
      this.name,
      this.gender,
      this.weight,
      this.fweight,
      this.height,
      this.birthdate,
      this.targetdrink});

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'gender': gender,
      'weight': weight,
      'target_weight': fweight,
      'height': height,
      'birthdate': birthdate,
      'target_drink': targetdrink
    };
  }

  static List<Users> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Users(
          uid: maps[i]['uid'],
          name: maps[i]['name'],
          gender: maps[i]['gender'],
          weight: maps[i]['weight'],
          fweight: maps[i]['target_weight'],
          height: maps[i]['height'],
          birthdate: maps[i]['birthdate'],
          targetdrink: maps[i]['target_drink']);
    });
  }

  @override
  String toString() {
    return 'User {id: $uid, name: $name, gender: $gender}';
  }
}
