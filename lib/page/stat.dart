import 'package:flutter/material.dart';
import 'package:healthy_tracker/components/weight_chart.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:easy_localization/easy_localization.dart';

class StatPage extends StatefulWidget {
  StatPage({Key? key, required this.name, required this.statWeight})
      : super(key: key);
  final String name;
  final List statWeight;
  @override
  _StatPageState createState() => _StatPageState();
}

class _StatPageState extends State<StatPage> {
  String name = '';
  List<dynamic> statWeight = [];
  @override
  void initState() {
    super.initState();
    name = widget.name;
    statWeight = widget.statWeight.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('stat.title'.tr()),
      ),
      body: SlidingUpPanel(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0),
          topRight: Radius.circular(24.0),
        ),
        collapsed: Container(
          height: 30,
          color: Colors.blueGrey,
          child: Center(
            child: Text(
              'stat.slide'.tr(),
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        panel: Center(
          child: WeightChartPage(data: statWeight),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                'stat.name'.tr(args: [name]),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 26,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 56.0),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.circular(18),
                ),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: statWeight.length,
                  itemBuilder: (context, int i) {
                    return Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Text(
                                '${DateFormat('dd/MM/yyyy').format(statWeight[i]['date'].toDate())}',
                                style: TextStyle(fontSize: 16),
                              ),
                              Text(
                                  '(${DateFormat('HH:mm').format(statWeight[i]['date'].toDate())})',
                                  style:
                                      TextStyle(color: Colors.grey.shade500)),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                '${statWeight[i]['weight']}',
                                style: TextStyle(fontSize: 20),
                              ),
                              (i == statWeight.length - 1)
                                  ? Text('+ 0.00')
                                  : ((statWeight[i]['weight'] -
                                              statWeight[i + 1]['weight']) <
                                          0)
                                      ? Text(
                                          '-${(statWeight[i + 1]['weight'] - statWeight[i]['weight']).toStringAsFixed(2)}',
                                          style: TextStyle(
                                              color: Colors.lightGreen),
                                          textAlign: TextAlign.left,
                                        )
                                      : Text(
                                          '+${(statWeight[i + 1]['weight'] - statWeight[i]['weight']).abs().toStringAsFixed(2)}',
                                          style: TextStyle(color: Colors.red),
                                          textAlign: TextAlign.left,
                                        ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
