import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/components/drawer.dart';
import 'package:healthy_tracker/controller/users.dart';
import 'package:healthy_tracker/model/users.dart';

class SettingPage extends StatefulWidget {
  SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  User? user = FirebaseAuth.instance.currentUser;
  Users dataUser = Users(name: 'Unknown', fweight: 0.0);
  double target_weight = 0.0;
  String name = '';
  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  Future<void> getDataUser() async {
    await getData().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyDrawer(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(16.0),
                        child: Image.network(
                          '${user!.photoURL}',
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        '${dataUser.name}',
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      ),
                      Text(
                        '(${user!.displayName})',
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      ),
                      Text(
                        '(${user!.email})',
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(
                    'setting.account'.tr(),
                    style:
                        TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: 0.7, color: Colors.black.withOpacity(0.2)),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                'setting.dialog2.title'.tr(),
                                style: TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                              ),
                              content: TextFormField(
                                initialValue: dataUser.name,
                                keyboardType: TextInputType.text,
                                onChanged: (value) {
                                  setState(() {
                                    name = value;
                                  });
                                },
                              ),
                              actions: [
                                Container(
                                  padding: EdgeInsets.all(16.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.red,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context, 'cancel'.tr());
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'cancel'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.green,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (name.length > 1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(new SnackBar(
                                              content: new Text(
                                                  'setting.dialog2.success'
                                                      .tr()),
                                              backgroundColor: Colors.green,
                                            ));
                                            await updateName(
                                                dataUser.docId!, name);
                                            setState(() {
                                              getDataUser();
                                            });
                                            Navigator.pop(context, 'save'.tr());
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(new SnackBar(
                                              content: new Text(
                                                  'setting.dialog2.error'.tr()),
                                              backgroundColor: Colors.red,
                                              duration: Duration(seconds: 1),
                                            ));
                                          }
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'save'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Icon(
                                    Icons.account_box_rounded,
                                    size: 40.0,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'setting.name'.tr(),
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    Text('${dataUser.name}')
                                  ],
                                ),
                              ],
                            ),
                            Icon(
                              Icons.chevron_right_rounded,
                              size: 30.0,
                            ),
                          ],
                        ),
                      ),
                      Divider(),
                      InkWell(
                        highlightColor: Colors.red,
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                'setting.dialog1.title'.tr(),
                                style: TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                              ),
                              content: TextFormField(
                                initialValue: dataUser.fweight.toString(),
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  setState(() {
                                    target_weight = double.parse(value);
                                  });
                                },
                              ),
                              actions: [
                                Container(
                                  padding: EdgeInsets.all(16.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.red,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context, 'cancel'.tr());
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'cancel'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.green,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () async {
                                          if (target_weight > 10) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(new SnackBar(
                                              content: new Text(
                                                  'setting.dialog1.success'
                                                      .tr()),
                                              backgroundColor: Colors.green,
                                            ));
                                            await updateTargetWeight(
                                                dataUser.docId!, target_weight);
                                            setState(() {
                                              getDataUser();
                                            });
                                            Navigator.pop(context, 'save'.tr());
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(new SnackBar(
                                              content: new Text(
                                                  'setting.dialog1.error'.tr()),
                                              backgroundColor: Colors.red,
                                              duration: Duration(seconds: 1),
                                            ));
                                          }
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'save'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Icon(
                                    Icons.monitor_weight,
                                    size: 40.0,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'setting.target_weight'.tr(),
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    Text('${dataUser.fweight}')
                                  ],
                                ),
                              ],
                            ),
                            Icon(
                              Icons.chevron_right_rounded,
                              size: 30.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    'setting.other'.tr(),
                    style:
                        TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                        width: 0.7, color: Colors.black.withOpacity(0.2)),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Icon(
                                    Icons.language,
                                    size: 40.0,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'setting.lang'.tr(),
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.green,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          6), // <-- Radius
                                    ),
                                  ),
                                  child: Text(
                                    'setting.en'.tr(),
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onPressed: () {
                                    context.setLocale(Locale('en'));
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(new SnackBar(
                                      content:
                                          new Text('setting.changelang'.tr()),
                                      backgroundColor: Colors.green,
                                    ));
                                  },
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                ElevatedButton(
                                  child: Text(
                                    'setting.th'.tr(),
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onPressed: () {
                                    context.setLocale(Locale('th'));
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(new SnackBar(
                                      content:
                                          new Text('setting.changelang'.tr()),
                                      backgroundColor: Colors.green,
                                    ));
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Divider(),
                      InkWell(
                        highlightColor: Colors.red,
                        onTap: () {
                          FirebaseAuth.instance.signOut();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Icon(
                                    Icons.logout_rounded,
                                    size: 40.0,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'menu.logout'.tr(),
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Icon(
                              Icons.chevron_right_rounded,
                              size: 30.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(),
              ],
            ),
          ),
        ),
        title: Text('setting.title'.tr()));
  }
}
