import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/controller/users.dart';
import 'package:healthy_tracker/model/users.dart';
import 'package:date_time_picker/date_time_picker.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String gender = 'ชาย';
  String name = '';
  double weight = 0.0;
  double fweight = 0.0;
  double height = 0.0;
  late Users newUser;
  DateTime birthdate = DateTime.now();
  final _formKey = GlobalKey<FormState>();

  // ignore: avoid_init_to_null
  User? _user = null;

  @override
  void initState() {
    super.initState();
    setState(() {
      _user = FirebaseAuth.instance.currentUser;
      hasRegistered(_user!.uid, context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('ลงทะเบียนเข้าใช้งาน'),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(16.0),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formKey,
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          child: TextFormField(
                            initialValue: _user!.displayName,
                            readOnly: true,
                            onChanged: (value) {
                              this.name = value;
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'กรุณาใส่ชื่อเล่นของคุณ';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                labelText: 'ชื่อ',
                                helperText: 'ใช้สำหรับชื่อเล่นในแอป'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('เพศ'),
                                DropdownButton(
                                  hint: Text('เพศ'),
                                  value: gender,
                                  onChanged: (String? value) {
                                    setState(() {
                                      gender = value!;
                                    });
                                  },
                                  items: [
                                    DropdownMenuItem(
                                      child: Text('ชาย'),
                                      value: 'ชาย',
                                    ),
                                    DropdownMenuItem(
                                      child: Text('หญิง'),
                                      value: 'หญิง',
                                    ),
                                  ],
                                ),
                              ],
                            )),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              this.weight = double.parse(value);
                            },
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  num.tryParse(value) == null) {
                                return 'กรุณาใส่น้ำหนักของคุณ';
                              }
                              return null;
                            },
                            decoration:
                                InputDecoration(labelText: 'น้ำหนักของคุณ'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              this.fweight = double.parse(value);
                            },
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  num.tryParse(value) == null) {
                                return 'กรุณาใส่น้ำหนักที่ตั้งไว้';
                              }
                              return null;
                            },
                            decoration:
                                InputDecoration(labelText: 'น้ำหนักที่ตั้งไว้'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              this.height = double.parse(value);
                            },
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  num.tryParse(value) == null) {
                                return 'กรุณาใส่ส่วนสูง';
                              }
                              return null;
                            },
                            decoration: InputDecoration(labelText: 'ส่วนสูง'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                DateTimePicker(
                  type: DateTimePickerType.date,
                  dateMask: 'dd/MM/yyyy',
                  icon: Icon(Icons.date_range),
                  dateLabelText: 'Date',
                  firstDate: DateTime(1950),
                  lastDate: DateTime(2100),
                  onChanged: (val) {
                    birthdate = DateTime.parse(val);
                  },
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: ElevatedButton.icon(
                      icon: Icon(Icons.save),
                      style: ElevatedButton.styleFrom(
                          elevation: 5,
                          shadowColor: Colors.grey,
                          primary: Colors.green[800],
                          textStyle: TextStyle(fontSize: 24)),
                      label: Text('บันทึก'),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          newUser = Users(
                              uid: _user!.uid,
                              name: _user!.displayName,
                              gender: gender,
                              weight: weight,
                              fweight: fweight,
                              height: height,
                              birthdate: birthdate);
                          addUser(newUser);
                          hasRegistered(_user!.uid, context);
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
