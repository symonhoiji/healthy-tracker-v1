import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/components/bmi_detail.dart';
import 'package:healthy_tracker/components/drawer.dart';
import 'package:healthy_tracker/controller/users.dart';
import 'package:healthy_tracker/page/stat.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String name = 't';
  double target_weight = 0.0;
  late List<dynamic> stat_weight = [];
  double latest_weight = 0.0;
  double sum_weight = 0.0;
  String dateNow = '';
  double bmi = 0.0;
  double height = 0.0;
  String docId = '';
  final _weight = TextEditingController();
  double minWeight = double.infinity;
  double averageWeight = 0.0;
  double bmr = 0.0;

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _addWeight(weight) async {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    await users.doc(docId).update({
      'stat_weight': FieldValue.arrayUnion([
        {'weight': weight, 'date': new DateTime.now()}
      ])
    });
    _weight.text = '';
    _loadProfile();
  }

  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  Future<void> _loadProfile() async {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    users.where('uid', isEqualTo: user!.uid).get().then((value) {
      value.docs.forEach((data) {
        setState(() {
          docId = data.id;
          name = data['full_name'];
          target_weight = data['target_weight'].toDouble();

          stat_weight = data['stat_weight'];

          double tmp = 0.0;
          stat_weight.forEach((element) {
            minWeight = (minWeight > element['weight'].toDouble())
                ? element['weight'].toDouble()
                : minWeight;
            tmp += element['weight'].toDouble();
          });
          averageWeight = (tmp / stat_weight.length);

          if (stat_weight.length >= 2) {
            sum_weight =
                stat_weight[stat_weight.length - 2]['weight'].toDouble() -
                    stat_weight[stat_weight.length - 1]['weight'].toDouble();
          }
          height = data['height'].toDouble();
          latest_weight = stat_weight.last['weight'].toDouble();
          dateNow = DateFormat('dd/MM/yyyy')
              .format(stat_weight.last['date'].toDate());
          bmi = (latest_weight / pow(height / 100, 2));
          if (data['gender'] == 'ชาย') {
            bmr = 88.362 +
                (13.397 * latest_weight) +
                (4.799 * height) -
                (5.677 * calculateAge(data['birthdate'].toDate()));
          } else {
            bmr = 447.593 +
                (9.247 * latest_weight) +
                (3.098 * height) -
                (4.330 * calculateAge(data['birthdate'].toDate()));
          }
        });
      });
    });
  }

  Widget checkBMI(n) {
    if (n < 18.50) {
      return Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: Colors.brown.shade700,
            borderRadius: BorderRadius.circular(16)),
        child:
            Text('home.bmi.level1'.tr(), style: TextStyle(color: Colors.white)),
      );
    } else if (n >= 18.50 && n < 24) {
      return Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: Colors.lightGreen.shade700,
            borderRadius: BorderRadius.circular(16)),
        child:
            Text('home.bmi.level2'.tr(), style: TextStyle(color: Colors.white)),
      );
    } else if (n >= 24 && n < 30) {
      return Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: Colors.orange.shade800,
            borderRadius: BorderRadius.circular(16)),
        child:
            Text('home.bmi.level3'.tr(), style: TextStyle(color: Colors.white)),
      );
    } else {
      return Container(
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: Colors.red.shade900,
            borderRadius: BorderRadius.circular(16)),
        child:
            Text('home.bmi.level4'.tr(), style: TextStyle(color: Colors.white)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MyDrawer(
      title: Text('home.title'.tr()),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                    // border: Border.all(width: 2),
                    color: Colors.grey.shade800,
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                alignment: Alignment.topRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: Image.network(
                        '${user!.photoURL}',
                        height: 60,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'home.body.hello'.tr(),
                          style: TextStyle(fontSize: 22, color: Colors.white),
                          textAlign: TextAlign.left,
                        ),
                        Text(
                          '$name',
                          style: TextStyle(fontSize: 22, color: Colors.white),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.all(16.0),
                width: double.infinity,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: RawMaterialButton(
                        onPressed: () async {
                          await showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                'home.input.title'.tr(),
                                style: TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                              ),
                              content: TextFormField(
                                  keyboardType: TextInputType.number,
                                  controller: _weight,
                                  decoration: InputDecoration(
                                      hintText: 'home.input.hintText'.tr())),
                              actions: [
                                Container(
                                  padding: EdgeInsets.all(16.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.red,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () => Navigator.pop(
                                            context, 'cancel'.tr()),
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'cancel'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                      ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          primary: Colors.green,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                18), // <-- Radius
                                          ),
                                        ),
                                        onPressed: () {
                                          _addWeight(
                                              double.parse(_weight.text));
                                          Navigator.pop(context, 'save'.tr());
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'save'.tr(),
                                            style: TextStyle(fontSize: 16),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        elevation: 2.0,
                        fillColor: Colors.green[500],
                        child: Column(
                          children: [
                            Text(
                              '$latest_weight',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 45,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Kg',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 19,
                              ),
                            ),
                            Text(
                              'home.input.sub'.tr(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(50.0),
                        shape: CircleBorder(),
                      ),
                    ),
                    Column(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (!sum_weight.isNegative)
                                ? Text(
                                    '- ${sum_weight.toStringAsFixed(2)}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.green),
                                  )
                                : Text(
                                    '+ ${sum_weight.abs().toStringAsFixed(2)}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red.shade900),
                                  ),
                            Text(
                              'home.middle.update'.tr(),
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            ),
                            Text(
                              '($dateNow)',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '$minWeight',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'home.middle.month'.tr(),
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            ),
                            Text(
                              '(Kg)',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '$target_weight',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'home.middle.target'.tr(),
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            ),
                            Text(
                              '(Kg)',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.grey[600]),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    border: Border.all(width: 0.3, color: Colors.grey),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    )),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => StatPage(
                                    name: name, statWeight: stat_weight)));
                      },
                      child: Container(
                        padding: EdgeInsets.all(16.0),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade800,
                            border: Border.all(width: 0.3, color: Colors.grey),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            )),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '$dateNow',
                              style: TextStyle(color: Colors.white),
                            ),
                            Text('home.bottom.stat'.tr(),
                                style: TextStyle(color: Colors.white)),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 12),
                    Column(
                      children: [
                        ExpansionTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'home.bottom.weight'.tr(),
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    '${averageWeight.toStringAsFixed(2)} Kg',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                ],
                              )
                            ],
                          ),
                          // trailing: SizedBox.shrink(),
                          // expandedAlignment: Alignment.centerRight,
                          childrenPadding: EdgeInsets.all(10.0),
                          textColor: Colors.brown,

                          children: [Text('home.bottom.weightdetail'.tr())],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        ExpansionTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'BMI',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    '${bmi.toStringAsFixed(1)}',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  checkBMI(bmi),
                                ],
                              )
                            ],
                          ),
                          // trailing: SizedBox.shrink(),
                          children: [
                            BMIDetail(bmi: bmi),
                          ],
                        ),
                        ExpansionTile(
                          childrenPadding: EdgeInsets.all(24.0),
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'BMR',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'home.bottom.cal'
                                        .tr(args: ['${bmr.toInt()}']),
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  )
                                ],
                              )
                            ],
                          ),
                          // trailing: SizedBox.shrink(),
                          children: [Text('home.bottom.bmr'.tr())],
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
