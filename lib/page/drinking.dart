import 'package:easy_localization/src/public_ext.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:healthy_tracker/components/drawer.dart';
import 'package:healthy_tracker/components/list_drinking.dart';
import 'package:healthy_tracker/controller/users.dart';
import 'package:healthy_tracker/model/users.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class DrinkingWaterPage extends StatefulWidget {
  DrinkingWaterPage({Key? key}) : super(key: key);

  @override
  _DrinkingWaterPageState createState() => _DrinkingWaterPageState();
}

int calculateDifference(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day)
      .difference(DateTime(now.year, now.month, now.day))
      .inDays;
}

class _DrinkingWaterPageState extends State<DrinkingWaterPage> {
  User? user = FirebaseAuth.instance.currentUser;
  late Users dataUser = Users(name: 'Unknown');
  double targetDrink = 0.0;
  List<int> keys = [];
  List<int> listDrink = [];
  String docId = "";
  int sum = 0;
  double quantity = 0.0;
  String uid = '';
  String date = DateFormat('dd/MM/yyyy').format(DateTime.now());

  @override
  void initState() {
    super.initState();
    _loadPage();
    _getDataUser();
  }

  Future<void> _getDataUser() async {
    await getData().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  Future<void> _loadPage() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      targetDrink = prefs.getDouble('targetDrink') ?? 2000;
      _getWater();
    });
  }

  Future<void> _setTargetDrinking(double n) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble('targetDrink', n);
  }

  Future<void> _getWater() async {
    final prefs = await SharedPreferences.getInstance();
    List<String> list = prefs.getStringList('listDrink') ?? [];
    List<String> list_key = prefs.getStringList('keys') ?? [];
    listDrink = list.map(int.parse).toList();
    keys = list_key.map(int.parse).toList();

    if (list.length > 0) {
      sum = listDrink.reduce((a, b) => a + b);
    } else {
      sum = 0;
    }
  }

  Future<void> _saveWater() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      List<String> list = listDrink.map((i) => i.toString()).toList();
      List<String> list_key = keys.map((i) => i.toString()).toList();
      prefs.setStringList('listDrink', list);
      prefs.setStringList('keys', list_key);
      _getWater();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyDrawer(
      floatingButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return StatefulBuilder(
                  builder: (thisLowerContext, innerSetState) {
                return AlertDialog(
                  // alignment: Alignment.center,
                  title: Text(
                    'drink.dialog1.title'.tr(),
                    style: TextStyle(fontSize: 30),
                    textAlign: TextAlign.center,
                  ),
                  content: Container(
                    height: 200,
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/bottle.png',
                          height: 95,
                        ),
                        Text(
                          '${quantity.round()}',
                          style:
                              TextStyle(fontSize: 36, color: Colors.lightBlue),
                        ),
                        Slider(
                          value: quantity,
                          min: 0,
                          max: (targetDrink - sum).toDouble(),
                          activeColor: Colors.blue,
                          inactiveColor: Colors.lightBlue.shade100,
                          divisions: 20,
                          onChanged: (double value) {
                            innerSetState(() {
                              quantity = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  actions: [
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(18), // <-- Radius
                              ),
                            ),
                            onPressed: () =>
                                Navigator.pop(context, 'cancel'.tr()),
                            child: Container(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                'cancel'.tr(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(18), // <-- Radius
                              ),
                            ),
                            onPressed: () {
                              setState(() {
                                if (sum != targetDrink && quantity > 0) {
                                  listDrink.insert(0, quantity.round());
                                  keys.insert(0, listDrink.length - 1);
                                  quantity = 0;
                                  sum = listDrink.reduce((a, b) => a + b);
                                  _saveWater();
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(new SnackBar(
                                    content:
                                        new Text('drink.dialog1.success'.tr()),
                                    backgroundColor: Colors.green,
                                    duration: Duration(seconds: 1),
                                  ));
                                } else {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(new SnackBar(
                                    content:
                                        new Text('drink.dialog1.error'.tr()),
                                    backgroundColor: Colors.red,
                                    duration: Duration(seconds: 1),
                                  ));
                                }
                              });

                              Navigator.pop(context, 'save'.tr());
                            },
                            child: Container(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                'save'.tr(),
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                );
              });
            },
          );
        },
        tooltip: 'drink.dialog1.tooltip'.tr(),
        child: Image.asset(
          'assets/bottle.png',
          height: 90,
        ),
        backgroundColor: Colors.lightBlue,
      ),
      title: Text('drink.title'.tr()),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(25.0),
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${dataUser.name}',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      Text(
                        '$date',
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  ElevatedButton.icon(
                    icon: Icon(Icons.edit),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.brown,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9), // <-- Radius
                      ),
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text(
                            'drink.dialog2.title'.tr(),
                            style: TextStyle(fontSize: 30),
                            textAlign: TextAlign.center,
                          ),
                          content: TextFormField(
                            initialValue: targetDrink.toString(),
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              setState(() {
                                targetDrink = double.parse(value);
                                _setTargetDrinking(targetDrink);
                              });
                            },
                          ),
                          actions: [
                            Container(
                              padding: EdgeInsets.all(16.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.red,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            18), // <-- Radius
                                      ),
                                    ),
                                    onPressed: () =>
                                        Navigator.pop(context, 'cancel'.tr()),
                                    child: Container(
                                      padding: EdgeInsets.all(12.0),
                                      child: Text(
                                        'cancel'.tr(),
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.green,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            18), // <-- Radius
                                      ),
                                    ),
                                    onPressed: () {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(new SnackBar(
                                        content: new Text(
                                            'drink.dialog2.success'.tr()),
                                        backgroundColor: Colors.green,
                                      ));
                                      Navigator.pop(context, 'save'.tr());
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(12.0),
                                      child: Text(
                                        'save'.tr(),
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                    label: Text('drink.dialog2.button'.tr()),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '$sum',
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                        Text('/$targetDrink',
                            style: TextStyle(
                                fontSize: 50,
                                color: Colors.lightBlue,
                                fontWeight: FontWeight.bold)),
                        Text('ML')
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    height: 2,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: listDrink.length,
                itemBuilder: (context, int i) {
                  return Dismissible(
                    onDismissed: (direction) {
                      setState(() {
                        listDrink.removeAt(i);
                        keys.removeAt(i);
                        _saveWater();
                      });
                    },
                    background: Container(color: Colors.red),
                    key: ValueKey(listDrink[i].toString()),
                    child: ListDrinking(quantity: listDrink[i]),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
